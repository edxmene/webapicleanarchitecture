﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Domain.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string User { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Description { get; set; }
    }
}
