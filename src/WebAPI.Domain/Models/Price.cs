﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Domain.Models
{
    public class Price
    {
        public int Id { get; set; }
        public string Product { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
