﻿using System;

namespace WebAPI.Infrastructure.Repositories;

public record Event(int Id, DateTime Date, string Location, string Description);
public class EventInMemoryRepository : IEventRepository
{
    private List<Event> _events = new()
    {
        new Event(1,new DateTime(2022, 08, 11),"London","PhD conference."),
        new Event(2,new DateTime(2022, 09, 25), "Portugal", "Msc conference.")
    };
    public Event Add(Event newEvent)
    {
        _events.Add(newEvent);
        return newEvent;
    }

    public IEnumerable<Event> GetAll() => _events;

    public Event GetById(int Id) => _events.FirstOrDefault(e => e.Id == Id);

    public void Delete(int Id)
    {
        var eventToDelete = GetById(Id);
        if (eventToDelete == null)
        {
            throw new ArgumentException("No event exists with the given Id", nameof(Id));
        }
        _events.Remove(eventToDelete);
    }
}

