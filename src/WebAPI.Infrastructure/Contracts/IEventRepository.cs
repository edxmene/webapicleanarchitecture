﻿
namespace WebAPI.Infrastructure.Repositories
{
    public interface IEventRepository
    {
        Event Add(Event newEvent);
        void Delete(int Id);
        IEnumerable<Event> GetAll();
        Event GetById(int Id);
    }
}