﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Domain.Models;

namespace WebAPI.Infrastructure
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions options): base(options){ }
        public DbSet<Price>? Prices { get; set; }
        public DbSet<Log>? Logs { get; set; }
    }
}
