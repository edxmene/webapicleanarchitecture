﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Infrastructure.Repositories;

namespace WebAPI.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventRepository _eventRepository;

        public EventsController(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_eventRepository.GetAll());
        }

        [HttpGet("{id}", Name = nameof(GetById))]
        public IActionResult GetById(int Id)
        {
            var existingEvent = _eventRepository.GetById(Id);
            if(existingEvent == null) return NotFound();
            return Ok(existingEvent);
        }

        [HttpPost]
        public IActionResult Add([FromBody] Event newEvent)
        {
            if(newEvent.Id < 1)
            {
                return BadRequest("Invalid Id");
            }
            _eventRepository.Add(newEvent);
            return CreatedAtAction(nameof(GetById), new {Id = newEvent.Id}, newEvent);
        }

        [HttpDelete]
        [Route("{eventIdToDelete}")]
        public IActionResult Delete(int eventIdToDelete)
        {
            try
            {
                _eventRepository.Delete(eventIdToDelete);
            }
            catch (ArgumentException)
            {

                return NotFound();
            }

            return NoContent();
        }
    }
}
