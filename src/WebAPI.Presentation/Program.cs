using WebAPI.Infrastructure;
using WebAPI.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<IEventRepository, EventInMemoryRepository>();

IConfiguration configuration = builder.Configuration;
var stringConnection = "DefaultConnection";
var connectionConfiguration = configuration.GetConnectionString(stringConnection);
if (connectionConfiguration == null)
{
    throw new ArgumentNullException(nameof(connectionConfiguration).ToString(),
            message: $"{stringConnection} doesn't exist in your appsetings.json");
}
builder.Services.AddDbContext<ProductContext>(options => options.UseSqlServer(connectionConfiguration));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
